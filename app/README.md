# **Liki React Biolerplate V1.0.0**

## Setup

In order to compile the app localy without the errors, please check if you have `node >= 10.16.0` and `npm >= 6.9.0`. This project use `yarn` as a dependency manager, please instal the current version of yarn globaly.

Yarn: <br /> `npm i -g yarn`

After getting the yarn successfully run `yarn` to install the app dependencies.

## Run the app

To run the app in development mode run: <br />
`yarn start`

To run the app in development mode with tunnel (based on ngrok) run: <br />
`yarn start:tunnel`

To run the app in production mode run: <br />
`yarn start:production`

## Build the production app

To build the production app run: <br />
`yarn build`

To clear the build run: <br />
`yarn build:clean`

## Tests and analyze

To trigger all the tests run: <br />
`yarn test`

To display the test coverage run: <br />
`yarn test:coverage`

To clean the tests reports run: <br />
`yarn test:clean`

To analyze the production build run: <br />
`yarn analyze`

## Clean the app

To clean all tests, analyze and build related files run: <br />
`yarn clean:all`

### Package used in project

- [ ] [React 16.9](<[https://reactjs.org/](https://reactjs.org/)>)

- [ ] [React Router 4](https://github.com/ReactTraining/react-router)

- [ ] [React Intl](https://github.com/yahoo/react-intl)

- [ ] [Redux](http://redux.js.org/)

- [ ] [Redux Saga](https://redux-saga.github.io/redux-saga/)

- [ ] [Redux Sauce](https://github.com/skellock/reduxsauce/)

- [ ] [Reselect](https://github.com/reactjs/reselect)

- [ ] [Axios](https://github.com/axios/axios)

- [ ] [Ramda](https://github.com/ramda/ramda)

- [ ] [Webpack 4](https://webpack.js.org/)

- [ ] [Formik]([https://jaredpalmer.com/formik/](https://jaredpalmer.com/formik/)

- [ ] [Yup](https://github.com/jquense/yup)

- [ ] [Jest ](https://jestjs.io/)

- [ ] [Storybook](https://storybook.js.org/)

### Codestyle principles

- Component not using lifecycle hooks should be functional
- When component declare not required prop, should include default value
- Routes should not be nested. Except nested routes prepend with parent path, eg. new and edit forms, tabs, etc.
- Variables used in style should be named to refer which componet use it and what should set. Global variables is forbidded to use in styles.
