import { expect } from 'chai';

import { selectLocalesLanguage } from '../locales.selectors';

describe('Locales: selectors', () => {
  const language = 'en';

  const mockedState = {
    locales: {
      language: language,
    },
  };

  describe('selectLocalesLanguage', () => {
    it('should select language', () => {
      expect(selectLocalesLanguage(mockedState)).to.equal(language);
    });
  });
});
