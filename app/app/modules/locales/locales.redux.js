import { createReducer, createActions } from 'reduxsauce';

export const DEFAULT_LOCALE = 'en';

export const { Types: LocalesTypes, Creators: LocalesActions } = createActions({
  setLanguage: ['language'],
}, { prefix: 'LOCALES_' });

export const INITIAL_STATE = {
  language: null,
};

export const setLanguageHandler = (state = INITIAL_STATE, action) => ({
  ...state,
  language: action.language,
});

export const HANDLERS = {
  [LocalesTypes.SET_LANGUAGE]: setLanguageHandler,
};

export const reducer = createReducer(INITIAL_STATE, HANDLERS);

