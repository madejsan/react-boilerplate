import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faTrashAlt,
  faSortDown,
  faPlus,
  faEdit,
  faCheck,
  faTimes,
  faEye,
} from '@fortawesome/free-solid-svg-icons';

/**
 * Register app icons
 */
library.add(faTrashAlt, faSortDown, faPlus, faEdit, faCheck, faTimes, faEye);
